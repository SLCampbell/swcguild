function hideResults(){
	document.getElementById("results").style.display = "none";
}

function playGame() {
	var startBet = parseInt(document.getElementById("betSize").value);
	var betSize = startBet;
	var maxWon = startBet;
	var maxRoll = 0;

	for (var rollCount = 0; betSize > 0; rollCount++) {
		var d1 = Math.floor(Math.random() * 6) + 1;
		var d2 = Math.floor(Math.random() * 6) + 1;
		if(d1 + d2 == 7){
			betSize += 4;
			if(betSize > maxWon){
				maxWon = betSize;
				maxRoll = rollCount;
			}
		} else {
			betSize -= 1;
		}
	}
		document.getElementById("results").style.display = "block";
		document.getElementById("btnPlay").innerHTML = "Play Again";
		document.getElementById("startBet").innerHTML = startBet;
		document.getElementById("totalRolls").innerHTML = rollCount;
		document.getElementById("highWon").innerHTML = maxWon;
		document.getElementById("highRoll").innerHTML = maxRoll;
};